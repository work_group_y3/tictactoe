
import com.sun.source.tree.TryTree;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ทักช์ติโชค
 */
public class Tictactoe {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlaye = 'O';
    static int row, col;
    static Scanner sc = new Scanner(System.in);
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {
        try {

            showWelcome();
            while (true) {
                showTable();
                showTrun();
                intputRowCol();
                process();
                if (finish) {
                    break;
                }
            }

        } catch (Exception e) {
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table.length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println();
        }
    }

    private static void showTrun() {
        System.out.println("Trun " + currentPlaye);
    }

    private static void intputRowCol() {

        System.out.println("Please intput row,col:");
        row = sc.nextInt();
        col = sc.nextInt();

        if (checkTable()) {
            finish = true;
            showErorr();
        }

    }

    private static void process() {
        if (setTable()) {
            if (checkWin()) {
                finish = true;
                showWin();
                return;
            }
            if (checkDraw()) {
                finish = true;
                showDraw();
                return;
            }
            count++;
            switcPlayer();
        }
    }

    private static void showWin() {
        showTable();
        System.out.println(">>> " + currentPlaye + " Win" + " <<<");
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlaye;
        return true;
    }

    public static void switcPlayer() {
        if (currentPlaye == 'O') {
            currentPlaye = 'X';
        } else {
            currentPlaye = 'O';
        }
    }

    private static boolean checkWin() {
        if (checkVertice()) {
            return true;
        } else if (checkHorizontal()) {
            return true;
        } else if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkVertice() {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlaye) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizontal() {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlaye) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    private static boolean checkX1() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlaye) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlaye) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        return count == 8;
    }

    private static void showDraw() {
        showTable();
        System.out.println(">>> " + "Draw" + " <<<");
    }

    private static boolean checkTable() {
        if (row - 1 < 0 || row - 1 > 2 || col - 1 < 0 || col - 1 > 2 || row == '-' || col == '-') {
            return true;
        } else if (table[row - 1][col - 1] != '-') {
            return true;
        } else {
            return false;
        }
    }

    private static void showErorr() {
        System.out.println("Error intput over the table");
    }

}
